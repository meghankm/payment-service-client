package com.meghan.paymentconfigclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymentConfigClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaymentConfigClientApplication.class, args);
	}

}
