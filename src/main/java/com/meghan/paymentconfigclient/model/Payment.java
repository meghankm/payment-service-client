package com.meghan.paymentconfigclient.model;

import java.math.BigDecimal;

public class Payment {

    private Integer accountId;
    private String paymentId;
    private BigDecimal amount;
    private String dailyLimit;
    private String dbPort;

    public Payment(){}

    public Payment(Integer accountId, String paymentId, BigDecimal amount) {
        this.accountId = accountId;
        this.paymentId = paymentId;
        this.amount = amount;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getDailyLimit() {
        return dailyLimit;
    }

    public void setDailyLimit(String dailyLimit) {
        this.dailyLimit = dailyLimit;
    }

    public String getDbPort() {
        return dbPort;
    }

    public void setDbPort(String dbPort) {
        this.dbPort = dbPort;
    }
}
